" vim-go
let g:go_fmt_command = "goimports"
let g:go_highlight_functions = 1 " this was off by default
let g:go_highlight_function_calls = 1
let g:go_highlight_function_parameters = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_interfaces = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_code_completion_enabled = 0
let g:go_auto_type_info = 0 " show type of variable under the cursor in the status bar
let g:go_snippet_engine = "neosnippet"
nmap <buffer> <localleader>i <Plug>(go-install)
nmap <buffer> <localleader>b <Plug>(go-build)
nmap <buffer> <localleader>r <Plug>(go-run)
nmap <buffer> <localleader>t <Plug>(go-test)
nmap <buffer> <localleader>c <Plug>(go-coverage-toggle)
nmap <buffer> <localleader>e <Plug>(go-iferr)
nnoremap <buffer> <localleader>do :GoDoc<CR>
nnoremap <buffer> <localleader>de :GoDef<CR>
nnoremap <buffer> <localleader>f :lua vim.lsp.buf.formatting()<CR>
" autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting()
" coc
let b:coc_pairs_disabled = ['<']
