# dotfiles

./install


## Neovim

Not really Vim compatible any more. Also Neovim > 0.5

## Tmux

Simple config ripped from someone, with a nice colorscheme and sensible bindings.


## Bash / Profile

Currently;
* .bashrc for everything
* .bash_profile calls .bashrc

## dotfiles/bin and $HOME/bin

Both set in $PATH (see previous section)
Some scripts included that I find useful are in dotfiles/bin 
Scripts than should be edited before use are copied into $HOME/bin 
